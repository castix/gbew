FROM python:3.10

RUN groupadd --gid 10001 python \
    && useradd --uid 10001 --gid python --shell /bin/bash --create-home python
RUN pip install -U setuptools
RUN pip install -U pip
RUN pip install wheel

COPY gbew/setup.py /app/setup.py

RUN pip install uwsgi boto3

RUN pip install -e /app

# RUN pip install remote_pdb

RUN pip install tg.devtools alembic

RUN mkdir /sqlite && touch /sqlite/db /sqlite/db-wal /sqlite/db-journal && chown -R python:python /sqlite
RUN mkdir /logs && touch /logs/setup-app.log && chown python:python /logs/setup-app.log

COPY gbew /app

EXPOSE 8080

USER python
RUN ls -al /sqlite
WORKDIR /app

# 1 process 2 threads
CMD ["bin/bash" "/app/entrypoint.sh" "1" "2"]
