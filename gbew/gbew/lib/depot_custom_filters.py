from depot.io import utils
from depot.manager import DepotManager
from depot.fields.interfaces import FileFilter
from PIL import Image
from io import BytesIO
import math


class WithSquareThumbnailFilter(FileFilter):
    """Uploads a square thumbnail that doesn't keep the aspect ratio
    together with the file.

    Takes for granted that the file is an image.
    The resulting uploaded file will provide three additional
    properties named:

        - ``square_thumb_X_id`` -> The depot file id
        - ``square_thumb_X_path`` -> Where the file is available in depot
        - ``square_thumb_X_url`` -> Where the file is served.

    Where ``X`` is the resolution specified as ``size`` in the
    filter initialization. By default this is ``(128, 128)``?so
    you will get ``square_thumb_128x128_id``, ``square_thumb_128x128_url`` and
    so on.

    .. warning::

        Requires Pillow library

    """
    def __init__(self, size=(128, 128), format='PNG'):
        self.thumbnail_size = size
        self.thumbnail_format = format

    def on_save(self, uploaded_file):
        content = utils.file_from_content(uploaded_file.original_content)

        t = Image.open(content)

        ratio = float(t.size[0]) / t.size[1]
        if ratio < 1:  # portrait  # pragma: no cover
            new_size = self.thumbnail_size[0], int(math.ceil(self.thumbnail_size[1] / ratio))
            z = (new_size[1] - self.thumbnail_size[0]) / 2
            left = 0
            upper = z
            right = self.thumbnail_size[0]
            lower = z + self.thumbnail_size[0]

        else:  # landscape  # pragma: no cover
            new_size = int(math.ceil(self.thumbnail_size[0] * ratio)), self.thumbnail_size[0]
            z = (new_size[0] - self.thumbnail_size[0]) / 2
            left = z
            upper = 0
            right = z + self.thumbnail_size[0]
            lower = self.thumbnail_size[0]

        t = t.resize(new_size)

        box = (left, upper, right, lower)
        thumbnail = t.crop(box)

        thumbnail = thumbnail.convert('RGBA')
        thumbnail.format = self.thumbnail_format

        output = BytesIO()
        thumbnail.save(output, self.thumbnail_format)
        output.seek(0)

        thumb_name = 'square_thumb_%sx%s' % self.thumbnail_size
        thumb_file_name = '%s.%s' % (thumb_name, self.thumbnail_format.lower())
        thumb_path, thumb_id = uploaded_file.store_content(output, thumb_file_name)
        uploaded_file[thumb_name + '_id'] = thumb_id
        uploaded_file[thumb_name + '_path'] = thumb_path
        uploaded_file[thumb_name + '_url'] = DepotManager.get_middleware().url_for(thumb_path)
        uploaded_file[thumb_name + '_public_url'] = uploaded_file.depot.get(thumb_id).public_url


class WithLossyFilter(FileFilter):
    """Uploads a thumbnail together with the file.

    Takes for granted that the file is an image.
    The resulting uploaded file will provide three additional
    properties named:

        - ``thumb_X_id`` -> The depot file id
        - ``thumb_X_path`` -> Where the file is available in depot
        - ``thumb_X_url`` -> Where the file is served.

    Where ``X`` is the resolution specified as ``size`` in the
    filter initialization. By default this is ``(128, 128)``?so
    you will get ``thumb_128x128_id``, ``thumb_128x128_url`` and
    so on.

    .. warning::

        Requires Pillow library

    """
    def __init__(self, size=(128, 128), format='JPEG'):
        self.thumbnail_size = size
        self.thumbnail_format = format

    def on_save(self, uploaded_file):
        content = utils.file_from_content(uploaded_file.original_content)

        # https://stackoverflow.com/questions/9166400/convert-rgba-png-to-rgb-with-pil
        _thumbnail = Image.open(content)
        _thumbnail.load()
        _thumbnail.thumbnail(self.thumbnail_size, Image.BILINEAR)
        thumbnail = Image.new("RGB", _thumbnail.size, (255, 255, 255))
        thumbnail.format = self.thumbnail_format
        try:
            thumbnail.paste(_thumbnail, mask=_thumbnail.split()[3])
        except IndexError:
            thumbnail.paste(_thumbnail)

        output = BytesIO()
        thumbnail.save(output, self.thumbnail_format)
        output.seek(0)

        thumb_name = 'thumb_%sx%s' % self.thumbnail_size
        thumb_file_name = '%s.%s' % (thumb_name, self.thumbnail_format.lower())
        thumb_path, thumb_id = uploaded_file.store_content(output, thumb_file_name)
        uploaded_file[thumb_name + '_id'] = thumb_id
        uploaded_file[thumb_name + '_path'] = thumb_path
        uploaded_file[thumb_name + '_url'] = DepotManager.get_middleware().url_for(thumb_path)
        uploaded_file[thumb_name + '_public_url'] = uploaded_file.depot.get(thumb_id).public_url
