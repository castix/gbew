from depot.fields.upload import UploadedFile
from depot.io.interfaces import FileStorage
from tempfile import SpooledTemporaryFile
from depot.io import utils
from PIL import Image
import exifread


class UploadedFileExif(UploadedFile):
    """This rotates the images that have `Image Orientation` in their
    exif metadata. eg: pictures taken by iOS"""
    def process_content(self, content, filename=None, content_type=None):
        # As we are replacing the main file, we need to explicitly pass
        # the filename and content_type, so get them from the old content.
        _, filename, content_type = FileStorage.fileinfo(content)

        # Get a file object even if content was bytes
        content = utils.file_from_content(content)

        im = Image.open(content)  # PIL Image
        content.seek(0)  # as I need to read the metadata
        tags = exifread.process_file(content)  # EXIF metadata

        # Rotate the image if needed
        if "Image Orientation" in tags.keys():  # pragma: no cover
            orientation = tags["Image Orientation"]
            val = orientation.values
            if 5 in val:
                val += [4, 8]
            if 7 in val:
                val += [4, 6]
            if 3 in val:
                im = im.transpose(Image.ROTATE_180)
            if 4 in val:
                im = im.transpose(Image.FLIP_TOP_BOTTOM)
            if 6 in val:
                im = im.transpose(Image.ROTATE_270)
            if 8 in val:
                im = im.transpose(Image.ROTATE_90)

        content = SpooledTemporaryFile(utils.INMEMORY_FILESIZE)
        im.save(content, format=im.format or 'JPEG')

        # propagates the content in filters
        object.__setattr__(self, 'original_content', content)

        content.seek(0)  # before calling super seeks again
        super(UploadedFileExif, self).process_content(content, filename, content_type)
