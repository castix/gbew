from gbew.model import DBSession, Gara, Picture
from tw2.core import Validator, ValidationError
from logging import getLogger


logger = getLogger(__name__)


class EditionValidator(Validator):
    def _convert_to_python(self, value, state=None):
        try:
            return DBSession.query(Gara).filter_by(edizione=value).one()
        except Exception as e:
            logger.exception(e)
            raise ValidationError(e)


class PictureValidator(Validator):
    def _convert_to_python(self, value, state=None):
        try:
            return DBSession.query(Picture).filter_by(_id=value).one()
        except Exception as e:
            logger.exception(e)
            raise ValidationError(e)
