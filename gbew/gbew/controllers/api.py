# -*- coding: utf-8 -*-
"""Sample controller with all its actions protected."""
from tg import expose, flash, validate, require, config, abort
from tg.i18n import ugettext as _, lazy_ugettext as l_
from tg.predicates import has_permission, not_anonymous
from gbew.lib.validators import Validator, EditionValidator
from gbew.lib.base import BaseController
from gbew.model import Picture
from datetime import datetime
import magic

__all__ = ['APIController']


def file_size(_file):
    """Returns the _file size"""
    _file.file.seek(0, 2)
    size = _file.file.tell()
    _file.file.seek(0)
    return size


def check_file(_file, _type=None, max_size=0):
    """Check that the given cgi.FieldStorage _file does not exceed the max_size
    and has the rigth mimetypei, calls tg.abort with 400 and passthrough='json'
    when something fails"""

    # as you migth load the max_size from an ini file, we cast strings to int
    if not isinstance(max_size, int):
        max_size = int(max_size)

    # file size check
    if file_size(_file) > max_size:
        abort(400, passthrough='json', detail='file too big')

    # type check through python-magic
    mimetype = magic.from_buffer(_file.file.read(1024), mime=True)
    type_of_file = mimetype.rsplit('/', 1)[0]
    if _type != type_of_file:
        abort(400, passthrough='json', detail='%s files are not accepted' % mimetype)
    _file.file.seek(0)



class APIController(BaseController):

    # The predicate that must be met for all the actions in this controller:
    # allow_only = has_permission(
    #     'manage',
    #     msg=l_('Only for people with the "manage" permission')
    # )
    # allow_only = not_anonymous()

    @expose('gbew.templates.index')
    def index(self):
        """Let the user know that's visiting a protected controller."""
        flash(_("Secure Controller here"))
        return dict(page='index')

    @expose('gbew.templates.index')
    def some_where(self):
        """Let the user know that this action is protected too."""
        return dict(page='some_where')


    @expose('json')
    @require(not_anonymous())
    @validate(validators={
        'edizione': EditionValidator(),
        'immagine': Validator(required=True),
        'description': Validator(),
    })
    def new_picture(self, edizione, immagine, description):
        check_file(immagine,
                   _type='image',
                   max_size=config.get('max_image_size_in_bytes', 500000))

        image = Picture()
        image.gara_id = edizione._id
        image.gara =  edizione
        image.content = immagine
        image.description = description
        image.creation_date = datetime.utcnow()
        return dict(image_id=image._id, image_thumbnail_url=image.thumbnail_1024x1024_url)

