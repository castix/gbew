# -*- coding: utf-8 -*-
"""WSGI application initialization for gbew."""
from gbew.config.app_cfg import base_config
from depot.manager import DepotManager


__all__ = ['make_app']


def make_app(global_conf, **app_conf):
    """
    Set gbew up with the settings found in the PasteDeploy configuration
    file used.

    :param dict global_conf: The global settings for gbew
                             (those defined under the ``[DEFAULT]`` section).

    :return: The gbew application with all the relevant middleware
        loaded.

    This is the PasteDeploy factory for the gbew application.

    ``app_conf`` contains all the application-specific settings (those defined
    under ``[app:main]``.
    """
    app = base_config.make_wsgi_app(global_conf, app_conf, wrap_app=None)
    DepotManager._middleware = None
    app = DepotManager.make_middleware(app)
    # Wrap your final TurboGears 2 application with custom middleware here

    return app
