# -*- coding: utf-8 -*-
"""Setup the gbew application"""
from __future__ import print_function, unicode_literals
import transaction
from gbew import model
from os import getenv


def bootstrap(command, conf, vars):
    """Place any commands to setup gbew here"""

    # <websetup.bootstrap.before.auth
    from sqlalchemy.exc import IntegrityError
    try:
        u = model.User()
        u.user_name = 'manager'
        u.display_name = 'Example manager'
        u.email_address = 'manager@somedomain.com'
        u.password = getenv('PASSWORD_MANAGER', 'ric0tt4')

        model.DBSession.add(u)

        g = model.Group()
        g.group_name = 'managers'
        g.display_name = 'Managers Group'

        g.users.append(u)

        model.DBSession.add(g)

        p = model.Permission()
        p.permission_name = 'manage'
        p.description = 'This permission gives an administrative right'
        p.groups.append(g)

        model.DBSession.add(p)

        u1 = model.User()
        u1.user_name = 'presente'
        u1.display_name = 'Example editor'
        u1.email_address = 'editor@somedomain.com'
        u1.password = ''  # this is a public anonymous user

        model.DBSession.add(u1)

        g22 = model.Gara()
        g22.edizione = '19'

        model.DBSession.add(g22)

        # from remote_pdb import RemotePdb
        # RemotePdb('0.0.0.0', 6999).set_trace()
        model.DBSession.flush()
        transaction.commit()
    except IntegrityError:
        print('Warning, there was a problem adding your auth data, '
              'it may have already been added:')
        import traceback
        print(traceback.format_exc())
        transaction.abort()
        print('Continuing with bootstrapping...')

    # <websetup.bootstrap.after.auth>
