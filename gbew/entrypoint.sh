cd /app

INI_FILE=/app/docker.ini


# first of all we run the websetup to initialize the database
if gearbox setup-app -c $INI_FILE >> /logs/setup-app.log 2>&1 ; then
    echo "DATABASE INITIALIZED SUCCESSFULLY"
else
    echo "errno " $? "while initializing database. perhaps setup-app already done, otherwise see /logs/setup-app.log"
fi
set -e


# then we migrate using alembic first the aplication itself
# then all the pluggables
gearbox migrate -c $INI_FILE upgrade
gearbox migrate-pluggable -c $INI_FILE tgapp-tgcomments upgrade

# actual run using uwsgi
/usr/local/bin/uwsgi --http :8080 --paste config:$INI_FILE --enable-threads --thunder-lock --processes $1 --threads $2 --master --stats :9191
